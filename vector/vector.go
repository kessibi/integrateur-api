package vector

import (
	"math"
)

// Vector is a 3D vector
type Vector struct {
	X, Y, Z float64
}

// Point is a 3D point
type Point struct {
	X, Y, Z float64
}

// ProtoVector represents a protobuf vector <I,J,K>
type ProtoVector interface {
	GetI() float64
	GetJ() float64
	GetK() float64
}

// FromProto convert a protobuf vector to a simple vector
func FromProto(v ProtoVector) Vector {
	return Vector{
		X: v.GetI(),
		Y: v.GetJ(),
		Z: v.GetK(),
	}
}

// CrossProduct returns a normalized vector
func CrossProduct(v1, v2 Vector) Vector {
	return Vector{
		v1.Y*v2.Z - v1.Z*v2.Y,
		v1.Z*v2.X - v1.X*v2.Z,
		v1.X*v2.Y - v1.Y*v2.X,
	}
}

// DotProduct returns the dot product of v1 and v2
func DotProduct(v1, v2 Vector) float64 {
	return v1.X*v2.X + v1.Y*v2.Y + v1.Z*v2.Z
}

// VectSub substract v2 to v1
func VectSub(v1, v2 Vector) Vector {
	return Vector{
		v1.X - v2.X,
		v1.Y - v2.Y,
		v1.Z - v2.Z,
	}
}

// PointToVect returns a vector for p1 and p2
func PointToVect(p1, p2 Point) Vector {
	return Vector{
		p2.X - p1.X,
		p2.Y - p1.Y,
		p2.Z - p1.Z,
	}
}

// MidSegment returns the center of the segment defined by p1 and p2
func MidSegment(p1, p2 Point) Point {
	return Point{
		(p2.X + p1.X) / 2.,
		(p2.Y + p1.Y) / 2.,
		(p2.Z + p1.Z) / 2.,
	}
}

// Translation returns the translated point from p by v
func Translation(p Point, v Vector) Point {
	return Point{
		p.X + v.X,
		p.Y + v.Y,
		p.Z + v.Z,
	}
}

// Distance returns the distance between p1 and p2
func Distance(p1, p2 Point) float64 {
	return math.Sqrt((p2.X-p1.X)*(p2.X-p1.X) + (p2.Y-p1.Y)*(p2.Y-p1.Y) + (p2.Z-p1.Z)*(p2.Z-p1.Z))
}

// Scale returns a scaled vector
func Scale(v Vector, scale float64) Vector {
	return Vector{
		v.X * scale,
		v.Y * scale,
		v.Z * scale,
	}
}

// Mat3x4multmat4x1 returns p in coordinates defined in the world of mat
func Mat3x4multmat4x1(mat [4]Vector, p Point) Point {
	return Point{
		mat[0].X*p.X + mat[1].X*p.Y + mat[2].X*p.Z + mat[3].X,
		mat[0].Y*p.X + mat[1].Y*p.Y + mat[2].Y*p.Z + mat[3].Y,
		mat[0].Z*p.X + mat[1].Z*p.Y + mat[2].Z*p.Z + mat[3].Z,
	}
}

// Normalisation normalizes a Vector's values
func Normalisation(v *Vector) {
	var norme float64
	norme = math.Sqrt(DotProduct(*v, *v))
	v.X = v.X / norme
	v.Y = v.Y / norme
	v.Z = v.Z / norme
}

// NormeSquare returns the squared norm of a Vector
func NormeSquare(v Vector) float64 {
	var res float64
	res = v.X*v.X + v.Y*v.Y + v.Z*v.Z
	return res
}
