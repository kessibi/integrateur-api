#!/bin/sh

echo "initializing go modules..."
go mod init

echo "installing binary..."
go install ./httpServer

echo "done"

exec "$@"
