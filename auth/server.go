package auth

import (
	"net/http"
	"os"
	"strings"

	"github.com/go-pg/pg"
	"github.com/gorilla/mux"
)

// Server is the main structure of the auth server
type Server struct {
	Router        *mux.Router
	RemoteServers map[string]*GameServer
}

//options for database acces
var dbOpts = pg.Options{
	Addr:     os.Getenv("DB_URL"),
	User:     os.Getenv("DB_USER"),
	Password: os.Getenv("DB_PASSWORD"),
	Database: os.Getenv("DB_NAME"),
}

//return the token from a request
func getToken(r *http.Request) string {
	res := ""
	authHeader := strings.Split(r.Header.Get("Authorization"), "Bearer ")
	if len(authHeader) > 1 {
		res = authHeader[1]
	}
	return res
}
