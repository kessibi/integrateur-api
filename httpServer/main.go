package main

import (
	"log"
	"net/http"
	"os"

	"git.unistra.fr/integratron/integrateur-api/auth"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func main() {
	lflag := os.Getenv("LOG_REQUESTS")
	s := auth.Server{
		Router:        mux.NewRouter(),
		RemoteServers: make(map[string]*auth.GameServer),
	}

	s.InitAuthRoutes()
	log.Println("auth server initialized")
	if lflag == "true" {
		loggedRouter := handlers.LoggingHandler(os.Stdout, s.Router)
		http.ListenAndServe("0.0.0.0:3001", loggedRouter)
	} else {
		http.ListenAndServe("0.0.0.0:3001", s.Router)
	}
}
