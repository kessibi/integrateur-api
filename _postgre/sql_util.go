package main

import (
	"fmt"
	"log"

	"github.com/go-pg/pg"
)

type User struct {
	ID       int    `sql:"userid"`
	Username string `sql:"login"`
	Password string `sql:"password"`
}

func main() {

	db := pg.Connect(&pg.Options{
		Addr:     "127.0.0.1:5432",
		User:     "postgres",
		Password: "integratron",
		Database: "postgres",
	})

	defer db.Close()

	user := &User{
		ID:       1,
		Username: "test",
		Password: "james",
	}

	err := db.Insert(user)
	if err != nil {
		log.Fatal(err)
	}

	var msg string

	fmt.Println(msg)
}
