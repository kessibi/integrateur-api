package server

import (
	"errors"
	"log"
	"net"
	"strings"
)

// BUFSIZE is the default buffer size
const BUFSIZE = 4096

// Server defines the basic implementation of TCP and UDP Servers
// the handle function is defined in a separate file
type Server interface {
	Run() error
	Close() error
	HandleConnections()
}

// UDPServer holds the basic structure of a TCP Server
type UDPServer struct {
	addr   string
	port   int
	server *net.UDPConn
	name   string
}

/* ========== UDP Server ========== */

// Run starts the UDP Server
func (u *UDPServer) Run() (err error) {

	addr := net.UDPAddr{
		Port: u.port,
		IP:   net.ParseIP(u.addr),
	}

	u.server, err = net.ListenUDP("udp", &addr)
	if err != nil {
		return errors.New("could not listen on UDP")
	}

	log.Println("server started on", addr.IP, addr.Port)
	u.HandleConnections()
	return
}

// Close shuts down the UDP Server
func (u *UDPServer) Close() error {
	return u.server.Close()
}

// CreateServer creates a server (yes, really) with the specified parameters
// (protocol, address and port)
func CreateServer(protocol, addr string, port int, name string) (Server, error) {
	switch strings.ToLower(protocol) {
	case "udp":
		return &UDPServer{
			addr: addr,
			port: port,
			name: name,
		}, nil
	}
	return nil, errors.New("Invalid protocol given")
}
