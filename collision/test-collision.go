package collision

import (
	"git.unistra.fr/integratron/integrateur-api/barrier"
	"git.unistra.fr/integratron/integrateur-api/vector"
)

// NBPOINT is the number of points that represents a bike
const NBPOINT int = 4

// HighLeft represents the upper-left point of the bike
var HighLeft = vector.Point{X: -1.5, Y: 2., Z: 3.}

// HighRight represents the upper-left point of the bike
var HighRight = vector.Point{X: 1.5, Y: 2., Z: 3.}

// GroundLeft represents the upper-left point of the bike
var GroundLeft = vector.Point{X: -1.5, Y: 0., Z: 3.}

// GroundRight represents the upper-left point of the bike
var GroundRight = vector.Point{X: 1.5, Y: 0., Z: 3.}

// FrontPoint represents points at the front of the moto that are tested
// for collisions
type FrontPoint struct {
	FrontPoints [4][2]vector.Point
	NbIter      int
}

// Param : float a, float b
// return true if a and b are negative or if a and b are positive
func sameSigne(a, b float64) bool {
	return (a <= 0 && b <= 0) || (a > 0 && b > 0)
}

//initialise une structure FrontPoint
func initFrontPoint(points *FrontPoint) {
	points.NbIter = 0
}

// Param : float value
// return absolute value
func abs(value float64) float64 {
	if value >= 0 {
		return value
	}
	return -value
}

// IsCloseEnough checks if a barrier and a moto are close enough based on
// provided values diffX and diffY
func IsCloseEnough(bar, moto vector.Point, diffX, diffY float64) bool {
	if bar.X <= moto.X+diffX && bar.X >= moto.X-diffX {
		if bar.Y <= moto.Y+diffY && bar.Y >= moto.Y-diffY {
			return true
		}
	}
	return false
}

// IsFastEnough cheks the speed of the bike
// Without collision the slowest the bike can go is 35
// Retuns false if speed is too low
func IsFastEnough(speed vector.Vector) bool {
	if vector.NormeSquare(speed) <= 1225 {
		return false
	}
	return true

}

func isSegmentInTriangle(s1, s2, v1, v2, v3 vector.Point, diffX, diffY float64) bool {
	var t, x, x_, y_, result float64
	vect1 := vector.PointToVect(v1, v2)
	vect2 := vector.PointToVect(v1, v3)
	var inter vector.Point
	var det float64
	var mat2X2 [2][2]float64
	var mat1X2 [2]float64
	var normal, segment vector.Vector
	var origin vector.Point
	var d float64

	if !IsCloseEnough(v1, s1, diffX, diffY) {
		return false
	}

	//premiere partie : on teste si le segment appartient au plan

	normal = vector.CrossProduct(vect2, vect1)
	origin = vector.Point{X: 0., Y: 0., Z: 0.}
	d = vector.DotProduct(normal, vector.PointToVect(origin, v1))
	segment = vector.PointToVect(s1, s2)
	segmentP1 := vector.Vector{X: s1.X, Y: s1.Y, Z: s1.Z}
	t = (d - vector.DotProduct(segmentP1, normal)) / vector.DotProduct(segment, normal)

	if t >= 0. && t <= 1. {
		//deuxieme partie on teste si le segment appartient au triangle

		x = (d - vector.DotProduct(normal, vector.PointToVect(origin, s1))) / vector.DotProduct(normal, segment)
		inter = vector.Point{X: s1.X + segment.X*x, Y: s1.Y + segment.Y*x, Z: s1.Z + segment.Z*x}

		if (vect1.X*vect2.Y - vect1.Y*vect2.X) != 0. {
			det = vect1.X*vect2.Y - vect1.Y*vect2.X
			mat2X2[0][0] = vect2.Y / det
			mat2X2[0][1] = -vect2.X / det
			mat2X2[1][0] = -vect1.Y / det
			mat2X2[1][1] = vect1.X / det

			mat1X2[0] = inter.X - v1.X
			mat1X2[1] = inter.Y - v1.Y
		} else if (vect1.Y*vect2.Z - vect1.Z*vect2.Y) != 0. {
			det = vect1.Y*vect2.Z - vect1.Z*vect2.Y
			mat2X2[0][0] = vect2.Z / det
			mat2X2[0][1] = -vect2.Y / det
			mat2X2[1][0] = -vect1.Z / det
			mat2X2[1][1] = vect1.Y / det

			mat1X2[0] = inter.Y - v1.Y
			mat1X2[1] = inter.Z - v1.Z
		} else if (vect1.Z*vect2.X - vect1.X*vect2.Z) != 0. {
			det = vect1.Z*vect2.X - vect1.X*vect2.Z
			mat2X2[0][0] = vect2.X / det
			mat2X2[0][1] = -vect2.Z / det
			mat2X2[1][0] = -vect1.X / det
			mat2X2[1][1] = vect1.Z / det

			mat1X2[0] = inter.Z - v1.Z
			mat1X2[1] = inter.X - v1.X

		} else {
			return false
		}

		x_ = mat2X2[0][0]*mat1X2[0] + mat2X2[0][1]*mat1X2[1]
		y_ = mat2X2[1][0]*mat1X2[0] + mat2X2[1][1]*mat1X2[1]
		result = x_ + y_

		return result <= 1. && result >= 0. && x_ >= 0. && y_ >= 0.

	}
	return false
}

// CollideABarrier checks collision between a moto and a barrier
//down : points au sol de la barrière
//up : points en l'air de la barrière
//barriereLength : longueur de la barrière
//p1 et p2 : point formant le segment à tester
//teste si le segment [p1 ; p2] appartient à un des triangle de la barrière
func CollideABarrier(bar barrier.Barrier, p1 vector.Point, p2 vector.Point) bool {

	var curr int
	var curr1 int
	var diffX, diffY float64
	diffX = abs(p1.X - p2.X)
	diffY = abs(p1.Y - p2.Y)

	for i := 0; i < bar.CurrLen-1; i++ {
		curr = i + bar.Cursor
		curr = curr % bar.CurrLen
		curr1 = curr + 1
		curr1 = curr1 % bar.CurrLen
		if isSegmentInTriangle(p1, p2, bar.Down[curr], bar.Down[curr1], bar.Up[curr], diffX+6., diffY+6.) {
			return true
		}
		if isSegmentInTriangle(p1, p2, bar.Up[curr1], bar.Down[curr1], bar.Up[curr], diffX+6., diffY+6.) {
			return true
		}
	}
	return false
}

// BikeHasACollision tests between a bike and a barrier
//down : points au sol de la barrière
//up : points en l'air de la barrière
//wheel roue arrière de la moto
//vectX, vectY, vectZ vecteur reprsentant la position de la moto dans le repère
//frontPoint : points qui sont testés pour savoir s'il y a collision (points à l'avant de la moto)
//teste si une moto a une collision avec une barrière
func BikeHasACollision(bar barrier.Barrier, points *FrontPoint) bool {

	if points.NbIter != 0 {
		for i := 0; i < NBPOINT; i++ {
			if CollideABarrier(bar, points.FrontPoints[i][0], points.FrontPoints[i][1]) {
				return true
			}
		}
	}
	if CollideABarrier(bar, (*points).FrontPoints[0][1], (*points).FrontPoints[1][1]) {
		return true
	}
	points.NbIter++
	return false

}

// NewFrontPoint initializes FrontPoints based on a transform matrix
func NewFrontPoint(frontPoints *FrontPoint, mat [4]vector.Vector) {

	for i := 0; i < NBPOINT; i++ {
		(*frontPoints).FrontPoints[i][0] = (*frontPoints).FrontPoints[i][1]
	}

	(*frontPoints).FrontPoints[0][1] = vector.Mat3x4multmat4x1(mat, GroundLeft)
	(*frontPoints).FrontPoints[1][1] = vector.Mat3x4multmat4x1(mat, GroundRight)
	(*frontPoints).FrontPoints[2][1] = vector.Mat3x4multmat4x1(mat, HighLeft)
	(*frontPoints).FrontPoints[3][1] = vector.Mat3x4multmat4x1(mat, HighRight)

	(*frontPoints).NbIter++
}

// CollisionWithFrontBike tests collision between two bikes (front)
func CollisionWithFrontBike(frontPoints [4][2]vector.Point, otherBike [4][2]vector.Point) bool {
	var diffX, diffY float64

	for i := 0; i < NBPOINT; i++ {
		diffX = abs(frontPoints[i][0].X - frontPoints[i][1].X)
		diffY = abs(frontPoints[i][0].Y - frontPoints[i][1].Y)
		if isSegmentInTriangle(frontPoints[i][0], frontPoints[i][1], otherBike[0][1], otherBike[1][1], otherBike[2][1], diffX+10., diffY+10.) {
			return true
		}
		if isSegmentInTriangle(frontPoints[i][0], frontPoints[i][1], otherBike[3][1], otherBike[1][1], otherBike[2][1], diffX+10., diffY+10.) {
			return true
		}
	}
	if isSegmentInTriangle(frontPoints[0][1], frontPoints[1][1], otherBike[0][1], otherBike[1][1], otherBike[2][1], diffX+14., diffY+14.) {
		return true
	}
	if isSegmentInTriangle(frontPoints[0][1], frontPoints[1][1], otherBike[3][1], otherBike[1][1], otherBike[2][1], diffX+14., diffY+14.) {
		return true
	}
	return false
}

// CollisionWithSideBike tests collision between two bikes (side)
func CollisionWithSideBike(frontPoints [4][2]vector.Point, otherBike [4][2]vector.Point) bool {
	var diffX, diffY float64
	var bottom [4]vector.Point
	longueur := 6.
	var v1, v2, normal vector.Vector
	v1 = vector.PointToVect(otherBike[0][1], otherBike[1][1])
	v2 = vector.PointToVect(otherBike[1][1], otherBike[3][1])
	normal = vector.CrossProduct(v1, v2)
	vector.Normalisation(&normal)
	normal = vector.Scale(normal, longueur)
	for i := 0; i < 4; i++ {
		bottom[i] = vector.Translation(otherBike[i][1], normal)
	}

	for i := 0; i < 4; i++ {
		diffX = abs(frontPoints[i][0].X - frontPoints[i][1].X)
		diffY = abs(frontPoints[i][0].Y - frontPoints[i][1].Y)
		if isSegmentInTriangle(frontPoints[i][0], frontPoints[i][1], otherBike[0][1], bottom[0], otherBike[2][1], diffX+10., diffY+10.) {
			return true
		}
		if isSegmentInTriangle(frontPoints[i][0], frontPoints[i][1], otherBike[0][1], bottom[0], bottom[2], diffX+10., diffY+10.) {
			return true
		}
		if isSegmentInTriangle(frontPoints[i][0], frontPoints[i][1], otherBike[1][1], bottom[1], otherBike[3][1], diffX+10., diffY+10.) {
			return true
		}
		if isSegmentInTriangle(frontPoints[i][0], frontPoints[i][1], otherBike[1][1], bottom[1], bottom[3], diffX+10., diffY+10.) {
			return true
		}
	}
	if isSegmentInTriangle(frontPoints[0][0], frontPoints[1][1], otherBike[0][1], bottom[0], otherBike[2][1], diffX+14., diffY+14.) {
		return true
	}
	if isSegmentInTriangle(frontPoints[0][0], frontPoints[1][1], otherBike[0][1], bottom[0], bottom[2], diffX+14., diffY+14.) {
		return true
	}
	if isSegmentInTriangle(frontPoints[0][0], frontPoints[1][1], otherBike[1][1], bottom[1], otherBike[3][1], diffX+14., diffY+14.) {
		return true
	}
	if isSegmentInTriangle(frontPoints[0][0], frontPoints[1][1], otherBike[1][1], bottom[1], bottom[3], diffX+14., diffY+14.) {
		return true
	}
	return false
}
