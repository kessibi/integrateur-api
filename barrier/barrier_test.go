package barrier

import (
	"fmt"
	"testing"

	"git.unistra.fr/integratron/integrateur-api/vector"
)

func init() {
}

// Remarque

// Après freeBarrier => currlen = 0 et segfault si diff > 200
// une fois MaxSeq atteind => Modulo toujours = 0 ?

func TestBarrier(t *testing.T) {
	var bar Barrier
	// var curr int
	// var curr1 int
	InitBarrier(&bar, 200, 0, 0)
	var i int

	// for j = 1; j < 2; j++ {
	// 	AddPointToBarrier(&bar, vector.Point{X: -1., Y: 1000., Z: 0.5}, vector.Point{X: 0.5, Y: 0., Z: 0.}, i)
	// }
	// AddPointToBarrier(&bar, vector.Point{X: 0., Y: 0., Z: 0.}, vector.Point{X: 1.5, Y: 0., Z: 1.}, 2)

	// AddPointToBarrier(&bar, vector.Point{X: 3., Y: 2., Z: 3.}, vector.Point{X: 10. + 0.5, Y: 2., Z: 10.}, 10)

	fmt.Println("Curllen = ", bar.CurrLen)

	for i = 1; i <= 100; i++ {
		// 	//fmt.Println("cursor = ", bar.Cursor)
		//	fmt.Println("currlen =", bar.CurrLen)
		AddPointToBarrier(&bar, vector.Point{X: 1., Y: 1., Z: 1.0}, vector.Point{X: 1., Y: 1., Z: 1.0}, i)
		//fmt.Println(bar.Down[i-1])
	}

	// AddPointToBarrier(&bar, vector.Point{X: 0., Y: 0., Z: 0.}, vector.Point{X: 0., Y: 0., Z: 0.}, 300)
	// fmt.Println(bar.Down[96])

	//AddPointToBarrier(&bar, vector.Point{X: 0., Y: 0., Z: 0.}, vector.Point{X: 0., Y: 0., Z: 0.}, 600)
	FreeBarrier(&bar)

	//InitBarrier(&bar, 200, 300, 0)

	AddPointToBarrier(&bar, vector.Point{X: 1., Y: 1., Z: 1.0}, vector.Point{X: 1., Y: 1., Z: 1.0}, 150)
	for i = 1; i <= 50; i++ {
		fmt.Println(bar.Down[i-1])
	}
	// fmt.Println(bar.Down[101])
	//AddPointToBarrier(&bar, vector.Point{X: 1., Y: 1., Z: 1.0}, vector.Point{X: 1., Y: 1., Z: 1.0}, 5)

	// for i := 0; i < bar.CurrLen; i++ {
	// 	curr = i + bar.Cursor
	// 	curr = curr % bar.CurrLen
	// 	curr1 = curr + 1
	// 	curr1 = curr1 % bar.CurrLen
	// 	fmt.Println("cur =", curr)
	// 	fmt.Println(bar.Down[i])
	// }

}
