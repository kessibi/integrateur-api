package main

import (
	"log"
	"os"
	"strconv"

	"git.unistra.fr/integratron/integrateur-api/server"
)

func main() {
	port := os.Getenv("SERVER_PORT")
	if port == "" {
		port = "1234"
	}
	name := os.Getenv("SERVER_NAME")
	if name == "" {
		name = "Generic Game Server"
	}
	_p, err := strconv.Atoi(port)
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	udp, err := server.CreateServer("udp", "0.0.0.0", _p, name)
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	udp.Run()

}
