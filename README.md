# Projet Intégrateur - Serveur

Partie serveur du projet intégrateur 2019

Technologies utilisées :

* [Protobuf](https://developers.google.com/protocol-buffers/ "Protobuf Website")
* [Go](https://golang.org "Golang Website")

Backends :

* PostGRE

L'utilisation de Docker est conseillée :)