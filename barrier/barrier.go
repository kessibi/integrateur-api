package barrier

import (
	"git.unistra.fr/integratron/integrateur-api/vector"
)

// MAX_SEQ is the maximal value of a sequence number
const MAX_SEQ = 4096

// Barrier represents a game barrier with :
//  * Up : represente les points de la barriere qui sont en l'air
//  * Down : represete les points de la barriere qui sont au sol
//  * MaxLen : longueur maximale de la barriere
//  * CurrLen : longueur courrante de la barriere
//  * Cursor : position dans le buffer circulaire du prochain
//  * element à ajouter
//  * SeqPred : numero de sequence du dernier paquet reçu pour incrementer la barriere
type Barrier struct {
	Up      []vector.Point
	Down    []vector.Point
	MaxLen  int
	CurrLen int
	Cursor  int
	SeqPred int
	Id      int
}

// InitBarrier permet d'initialiser une barriere precedemment declaree
//  * bar : barriere a initialiser
//  * max : longueur max de la barrier
//  * seq : numero de sequence au moment de l'initialisation
func InitBarrier(bar *Barrier, max int, seq int, id int) {
	bar.Up = make([]vector.Point, max)
	bar.Down = make([]vector.Point, max)
	bar.CurrLen = 0
	bar.MaxLen = max
	bar.Cursor = 0
	bar.SeqPred = seq
	bar.Id = id
}

// FreeBarrier resets a barrier
func FreeBarrier(bar *Barrier) {
	bar.Up = nil
	bar.Down = nil
	InitBarrier(bar, bar.MaxLen, bar.SeqPred, bar.Id)
}

// AddPointToBarrier : ajout d'un point dans la barriere
//  * bar : barriere a incrementer
//  * p1 : point a ajouter dans Down
//  * p2 : point a ajouter dans Up
//  * seqNum : numero de sequence de la trame recue
func AddPointToBarrier(bar *Barrier, p1, p2 vector.Point, seqNum int) {
	var diff int
	//on determine le nombre de trame qui ont ete perdues
	//si diff = 1 alors aucune trame n'a été perdue
	//si diff = 0 alors la trame recue ne doit pas etre traitee
	if seqNum > bar.SeqPred {
		diff = seqNum - bar.SeqPred
	} else if seqNum < bar.SeqPred%(MAX_SEQ/2) { // bar.SeqPred%(MAX_SEQ/2))
		diff = MAX_SEQ - bar.SeqPred + seqNum
	} else {
		diff = 0
	}
	//fmt.Println("Diff = ", diff)
	if diff > 0 {
		//si la barriere est vide on rempli les diff premiere case avec le point recu
		if bar.CurrLen == 0 {
			// diff = (diff % bar.MaxLen) + 1
			// bar.Up[diff-1] = p2
			// bar.Down[diff-1] = p1
			for i := 0; i <= diff-1; i++ {
				bar.Up[(bar.Cursor+i)%bar.MaxLen] = p2
				bar.Down[(bar.Cursor+i)%bar.MaxLen] = p1
			}

			bar.CurrLen += diff
			if bar.CurrLen > bar.MaxLen {
				bar.CurrLen = bar.MaxLen
			}

			bar.Cursor = (bar.Cursor + diff) % bar.MaxLen
			bar.SeqPred = seqNum

		} else {
			//si diff est > 4 (plus de 4 trames perdues) on utilise l'interpolation circulaire
			//sinon on utilise l'interpolation lineaire
			if diff > 4 && bar.CurrLen >= 2 {
				InterpolationCirculaire(bar, diff, p1, p2)
			} else {
				InterpolationLineaire(bar, diff, p1, p2)
			}
			bar.Up[(bar.Cursor+diff-1)%bar.MaxLen] = p2
			bar.Down[(bar.Cursor+diff-1)%bar.MaxLen] = p1

			//on incremente la longueur et on met a jour le cursor et le numero de sequence
			bar.Cursor += diff
			bar.Cursor = bar.Cursor % bar.MaxLen
			bar.CurrLen += diff

			if bar.CurrLen > bar.MaxLen {
				bar.CurrLen = bar.MaxLen
			}

			bar.SeqPred = seqNum
		}
	}
}

/*
 * permet d'obtenir les coordonnées des points a interpoler
 * bar : barriere, sert a recupérer les deux points précédents
 * p : nouveau point, il faut interpoler entre ce point et les
 * dernier deja presents dans la barriere
 * t reel compris entre 0 et 1 permet d'obtenir le seuil d'interpolation
 */

func getSplinePoint(bar *Barrier, p vector.Point, t float64) (float64, float64) {
	var p0, p1 int
	if bar.Cursor == 0 {
		p1 = bar.CurrLen - 1
		p0 = bar.CurrLen - 2
	} else if bar.Cursor == 1 {
		p1 = 0
		p0 = bar.CurrLen - 1
	} else {
		p1 = bar.Cursor - 1
		p0 = bar.Cursor - 2
	}

	t = t - float64(int(t))

	tt := t * t
	ttt := tt * t

	q1 := -ttt + 2.*tt - t
	q2 := 3.*ttt - 5.*tt + 2.
	q3 := -3.*ttt + 4.*tt + t
	q4 := ttt - tt

	tx := 0.5 * (bar.Down[p0].X*q1 + bar.Down[p1].X*q2 + p.X*q3 + q4*p.X)
	tz := 0.5 * (bar.Down[p0].Z*q1 + bar.Down[p1].Z*q2 + p.Z*q3 + q4*p.Z)

	return tx, tz
}

// InterpolationCirculaire : permet d'effectuer une interpolation circulaire
//  * bar : barriere sur laquelle faire l'interpolation
//  * longueur : nombre de point a interpoler
//  * p1 : point du sol sur lequel interpoler
//  * p2 : point en l'air sur lequel interpoler
func InterpolationCirculaire(bar *Barrier, longueur int, p1, p2 vector.Point) {
	var tx, tz float64

	lgFloat := float64(longueur)

	for i := 0; i < longueur-1.; i++ {
		tx, tz = getSplinePoint(bar, p1, float64(i+1)/lgFloat)
		bar.Down[(bar.Cursor+i)%bar.MaxLen] = vector.Point{
			X: tx,
			Y: p2.Y,
			Z: tz,
		}
		tx, tz = getSplinePoint(bar, p2, float64(i+1)/lgFloat)
		bar.Up[(bar.Cursor+i)%bar.MaxLen] = vector.Point{
			X: tx,
			Y: p1.Y,
			Z: tz,
		}
	}
}

// InterpolationLineaire : permet d'effectuer ue interpolation lineaire
//  * bar : barriere sur laquelle interpoler
//  * longueur : nombre de point a interpoler
//  * p1 : point du sol sur lequel interpoler
//  * p2 : point en l'air sur lequel interpoler
func InterpolationLineaire(bar *Barrier, longueur int, p1, p2 vector.Point) {
	var vectDiffUp, vectDiffDown vector.Vector

	if bar.Cursor == 0 {
		vectDiffUp = vector.PointToVect(bar.Up[bar.MaxLen-1], p2)
		vectDiffDown = vector.PointToVect(bar.Down[bar.MaxLen-1], p1)
	} else {
		vectDiffUp = vector.PointToVect(bar.Up[(bar.Cursor-1)%bar.MaxLen], p2)
		vectDiffDown = vector.PointToVect(bar.Down[(bar.Cursor-1)%bar.MaxLen], p1)
	}

	vectDiffFloat := float64(longueur)

	for i := 0; i < longueur-1; i++ {
		bar.Up[(bar.Cursor+i)%bar.MaxLen] = vector.Point{
			X: p2.X - (vectDiffFloat-1-float64(i))*vectDiffUp.X/vectDiffFloat,
			Y: p2.Y - (vectDiffFloat-1-float64(i))*vectDiffUp.Y/vectDiffFloat,
			Z: p2.Z - (vectDiffFloat-1-float64(i))*vectDiffUp.Z/vectDiffFloat,
		}
		bar.Down[(bar.Cursor+i)%bar.MaxLen] = vector.Point{
			X: p1.X - (vectDiffFloat-1-float64(i))*vectDiffDown.X/vectDiffFloat,
			Y: p1.Y - (vectDiffFloat-1-float64(i))*vectDiffDown.Y/vectDiffFloat,
			Z: p1.Z - (vectDiffFloat-1-float64(i))*vectDiffDown.Z/vectDiffFloat,
		}
	}
}
