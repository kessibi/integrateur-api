package vector

import (
	"testing"
)

func init() {

}

func TestVector_CrossProduct(t *testing.T) {
	x := Vector{X: 4., Y: 10., Z: 2.}
	y := Vector{X: 2., Y: -6., Z: 0.}

	result := CrossProduct(x, y)

	if result.X != 12 || result.Y != 4 || result.Z != -44 {
		t.Errorf("result = (%f, %f, %f) but expected (12, 4, -44)",
			result.X, result.Y, result.Z)
	}
}

func TestVector_DotProduct(t *testing.T) {
	x := Vector{X: 2., Y: 2., Z: 0.}
	y := Vector{X: 2., Y: -2., Z: 1.}

	result := DotProduct(x, y)

	if result != 0 {
		t.Errorf("result = %f, expected 0", result)
	}

	x = Vector{X: 1., Y: 2., Z: 2.}
	y = Vector{X: -2., Y: 5., Z: 0.}

	result = DotProduct(x, y)

	if result != 8. {
		t.Errorf("result = %f, expected 8", result)
	}

}

func TestVector_VectSub(t *testing.T) {
	x := Vector{X: 1., Y: 2., Z: 2.}
	y := Vector{X: -2., Y: 5., Z: 0.}

	result := VectSub(x, y)

	if result.X != 3. || result.Y != -3. || result.Z != 2. {
		t.Errorf("result = (%f, %f, %f) but expected (3., -3., 2.)",
			result.X, result.Y, result.Z)
	}
}

func TestVector_PointToVect(t *testing.T) {
	x := Point{X: 1., Y: 2., Z: 2.}
	y := Point{X: -2., Y: 5., Z: 0.}

	result := PointToVect(x, y)

	if result.X != -3. || result.Y != 3. || result.Z != -2. {
		t.Errorf("result = (%f, %f, %f) but expected (-3., 3., -2.)",
			result.X, result.Y, result.Z)
	}
}

func TestVector_MidSegment(t *testing.T) {
	x := Point{X: 1., Y: 2., Z: 2.}
	y := Point{X: -2., Y: 5., Z: 0.}

	result := MidSegment(x, y)

	if result.X != -0.5 || result.Y != 3.5 || result.Z != 1. {
		t.Errorf("result = (%f, %f, %f) but expected (-0.5., 3.5, ..)",
			result.X, result.Y, result.Z)
	}
}
