INSERT INTO users values (1 /*DEFAULT can be used*/, 'doggy','jammy'); --success
INSERT INTO users values (1, 'kitty','james'); -- fail id already used
INSERT INTO users values (2, 'doggy','patty'); -- fail login already exist

select login('doggy','james'); ---fail wrong password
select login('kitty','james'); ---fail wrong login
select login('doggy','jammy'); --- success

INSERT INTO matchs values (1 /*DEFAULT can be used*/, 7,interval '10 minutes'); --success
INSERT INTO matchs values (1, 7,interval '10 minutes'); --fail match already exists
INSERT INTO matchs values (2, 3,interval '1 hours 2 minutes 3 seconds'); --success 

INSERT INTO matchkills values (1 , 1, 1); --success (suicide authorized)
INSERT INTO matchkills values (3,1,1); --fail match 3 doesn't exists

