DROP SCHEMA public CASCADE;
CREATE SCHEMA public;
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE TABLE users (
	id serial PRIMARY KEY UNIQUE,
    uuid text UNIQUE default(gen_random_uuid()),
	login VARCHAR(255) UNIQUE not null,
	password VARCHAR(255) not null,
    deathCount INTEGER default(0),
    killCount INTEGER default(0)
);

CREATE TABLE matchs (
    mode NUMERIC(3),
    timeLength INTERVAL
);
