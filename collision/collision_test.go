package collision

import (
	"testing"
	"fmt"
	"time"

	"git.unistra.fr/integratron/integrateur-api/barrier"
	"git.unistra.fr/integratron/integrateur-api/vector"
)

func init() {
}

func TestCollision_SegmentTriangleIn(t *testing.T) {
	var p1, p2 vector.Point
	var tri1, tri2, tri3 vector.Point

	p1 = vector.Point{X: 0., Y: 1.5, Z: 0.}
	p2 = vector.Point{X: 2., Y: 1.5, Z: 0.}
	tri1 = vector.Point{X: 1., Y: 1., Z: 0.}
	tri3 = vector.Point{X: 1., Y: 2., Z: 0.}
	tri2 = vector.Point{X: 1., Y: 1.5, Z: 2.}

	if !isSegmentInTriangle(p1, p2, tri1, tri2, tri3, 100., 100.) {
		t.Error("segment not in triangle (should be)")
	}
}

func TestCollision_SegmentTriangleOut(t *testing.T) {
	var p1, p2 vector.Point
	var tri1, tri2, tri3 vector.Point

	p1 = vector.Point{X: 0., Y: 1.5, Z: 0.}
	p2 = vector.Point{X: 0., Y: 3., Z: 0.}
	tri1 = vector.Point{X: 1., Y: 1., Z: 0.}
	tri3 = vector.Point{X: 1., Y: 2., Z: 0.}
	tri2 = vector.Point{X: 1., Y: 1.5, Z: 2.}

	if isSegmentInTriangle(p1, p2, tri1, tri2, tri3, 100., 100.) {
		t.Error("segment in triange (shoudn't be)")
	}
}

func TestCollision_SegmentTriangleFloorOut(t *testing.T) {
	var p1, p2 vector.Point
	var tri1, tri2, tri3 vector.Point

	p1 = vector.Point{X: 0., Y: 3., Z: 0.}
	p2 = vector.Point{X: 2., Y: 3., Z: 0.}
	tri1 = vector.Point{X: 1., Y: 1., Z: 0.}
	tri3 = vector.Point{X: 1., Y: 2., Z: 0.}
	tri2 = vector.Point{X: 1., Y: 1.5, Z: 2.}

	if isSegmentInTriangle(p1, p2, tri1, tri2, tri3, 100., 100.) {
		t.Error("segment in triange (shoudn't be)")
	}
}

func TestCollision_SegmentTriangleFloorIn(t *testing.T) {

	p1 := vector.Point{X: 0., Y: 2., Z: 1.}
	p2 := vector.Point{X: 2., Y: 2., Z: 1.}
	tri1 := vector.Point{X: 1., Y: 2.5, Z: 2.}
	tri3 := vector.Point{X: 1., Y: 2., Z: 0.}
	tri2 := vector.Point{X: 1., Y: 1.5, Z: 2.}

	if !isSegmentInTriangle(p1, p2, tri1, tri2, tri3, 100., 100.) {
		t.Error("segment not in triangle (should be)")
	}
}

func TestCollision_SegmentTriangleAerialOut(t *testing.T) {

	p1 := vector.Point{X: 0., Y: 1.5, Z: 0.}
	p2 := vector.Point{X: 0., Y: 3., Z: 0.}
	tri1 := vector.Point{X: 1., Y: 2.5, Z: 2.}
	tri3 := vector.Point{X: 1., Y: 2., Z: 0.}
	tri2 := vector.Point{X: 1., Y: 1.5, Z: 2.}

	if isSegmentInTriangle(p1, p2, tri1, tri2, tri3, 100., 100.) {
		t.Error("segment in triangle (shouldn't be)")
	}
}
func TestCollision_SegmentTriangleAerialIn(t *testing.T) {

	p1 := vector.Point{X: 0., Y: 2., Z: 1.}
	p2 := vector.Point{X: 2., Y: 2., Z: 1.}
	tri1 := vector.Point{X: 1., Y: 2.5, Z: 2.}
	tri3 := vector.Point{X: 1., Y: 2., Z: 0.}
	tri2 := vector.Point{X: 1., Y: 1.5, Z: 2.}

	if !isSegmentInTriangle(p1, p2, tri1, tri2, tri3, 100., 100.) {
		t.Error("segment not in triangle (should be)")
	}
}


func TestCollision_Chrono(t *testing.T) {

	nbPlayer := 9

	var bar barrier.Barrier
	barrier.InitBarrier(&bar, 500, 0, 0)

	vecX := vector.Vector{X: 1., Y: 0., Z: 0.}
	vecY := vector.Vector{X: 0., Y: 1., Z: 0.}
	vecZ := vector.Vector{X: 0., Y: 0., Z: 1.}
	vectWheel := vector.Vector{X: 2998.5, Y: 1., Z: 0.}

	var bikeFront [4][2]vector.Point
	bikeFront[0][0] = vector.Point{X: 3000., Y: 1., Z: 0.}
	bikeFront[1][0] = vector.Point{X: 3000., Y: 3., Z: 0.}
	bikeFront[2][0] = vector.Point{X: 3000., Y: 1., Z: 2.}
	bikeFront[3][0] = vector.Point{X: 3000., Y: 3., Z: 2.}

	mat := [4]vector.Vector{
		vecX,
		vecY,
		vecZ,
		vectWheel,
	}

	for i := 0; i < 500; i++ {
		barrier.AddPointToBarrier(&bar, vector.Point{
			X: float64(i), Y: 2., Z: 0.},
			vector.Point{
				X: float64(i) + 0.5, Y: 2.,
				Z: 2.,
			},
			i+1)
	}

	var bike FrontPoint
	bike.FrontPoints = bikeFront
	initFrontPoint(&bike)
	bike.NbIter++

	now := time.Now()

	for i := 0 ; i < 50 * nbPlayer ; i++ {
		NewFrontPoint(&bike, mat)
	}

	for i := 0 ; i < nbPlayer * nbPlayer * 50 ; i++ {
		if BikeHasACollision(bar, &bike) {
			t.Error("collision has been detected")
		}
	}

	now2 := time.Now()
	elapsed := now2.Sub(now)

	fmt.Println("temps total ecoulé : ", elapsed)
}



func TestCollision_CollisionWithFrontBike(t *testing.T) {

	var bike1 [4][2]vector.Point
	var bike2 [4][2]vector.Point

	bike1[0][0] = vector.Point{X: -1.5, Y: 2., Z: 0.}
	bike1[1][0] = vector.Point{X: 1.5, Y: 2., Z: 0.}
	bike1[2][0] = vector.Point{X: -1.5, Y: 0., Z: 0.}
	bike1[3][0] = vector.Point{X: 1.5, Y: 0., Z: 0.}

	bike1[0][1] = vector.Point{X: -1.5, Y: 2., Z: 2.}
	bike1[1][1] = vector.Point{X: 1.5, Y: 2., Z: 2.}
	bike1[2][1] = vector.Point{X: -1.5, Y: 0., Z: 2.}
	bike1[3][1] = vector.Point{X: 1.5, Y: 0., Z: 2.}

	bike2[0][0] = vector.Point{X: -1.5, Y: 2., Z: 3.}
	bike2[1][0] = vector.Point{X: 1.5, Y: 2., Z: 3.}
	bike2[2][0] = vector.Point{X: -1.5, Y: 0., Z: 3.}
	bike2[3][0] = vector.Point{X: 1.5, Y: 0., Z: 3.}

	bike2[0][1] = vector.Point{X: -1.5, Y: 2., Z: 1.}
	bike2[1][1] = vector.Point{X: 1.5, Y: 2., Z: 1.}
	bike2[2][1] = vector.Point{X: -1.5, Y: 0., Z: 1.}
	bike2[3][1] = vector.Point{X: 1.5, Y: 0., Z: 1.}

	if !CollisionWithFrontBike(bike1, bike2) {
		t.Error("collision should have been detected")
	}

	bike2[0][0] = vector.Point{X: -1.5, Y: 2., Z: 5.}
	bike2[1][0] = vector.Point{X: 1.5, Y: 2., Z: 5.}
	bike2[2][0] = vector.Point{X: -1.5, Y: 0., Z: 5.}
	bike2[3][0] = vector.Point{X: 1.5, Y: 0., Z: 5.}

	bike2[0][1] = vector.Point{X: -1.5, Y: 2., Z: 3.}
	bike2[1][1] = vector.Point{X: 1.5, Y: 2., Z: 3.}
	bike2[2][1] = vector.Point{X: -1.5, Y: 0., Z: 3.}
	bike2[3][1] = vector.Point{X: 1.5, Y: 0., Z: 3.}

	if CollisionWithFrontBike(bike1, bike2) {
		t.Error("Collision should not have been detected")
	}

}

func TestCollision_CollisionWithSideBike(t *testing.T) {

	var bike1 [4][2]vector.Point
	var bike2 [4][2]vector.Point

	bike1[0][0] = vector.Point{X: -1.5, Y: 2., Z: 0.}
	bike1[1][0] = vector.Point{X: 1.5, Y: 2., Z: 0.}
	bike1[2][0] = vector.Point{X: -1.5, Y: 0., Z: 0.}
	bike1[3][0] = vector.Point{X: 1.5, Y: 0., Z: 0.}

	bike1[0][1] = vector.Point{X: -3.5, Y: 2., Z: 2.}
	bike1[1][1] = vector.Point{X: -1.5, Y: 2., Z: 2.}
	bike1[2][1] = vector.Point{X: -3.5, Y: 0., Z: 2.}
	bike1[3][1] = vector.Point{X: -1.5, Y: 0., Z: 2.}

	bike2[0][0] = vector.Point{X: -4.5, Y: 2., Z: 0.}
	bike2[1][0] = vector.Point{X: -2.5, Y: 2., Z: 0.}
	bike2[2][0] = vector.Point{X: -4.5, Y: 0., Z: 0.}
	bike2[3][0] = vector.Point{X: -2.5, Y: 0., Z: 0.}

	bike2[0][1] = vector.Point{X: -2.5, Y: 2., Z: 2.}
	bike2[1][1] = vector.Point{X: -0.5, Y: 2., Z: 2.}
	bike2[2][1] = vector.Point{X: -2.5, Y: 0., Z: 2.}
	bike2[3][1] = vector.Point{X: -0.5, Y: 0., Z: 2.}

	if !CollisionWithSideBike(bike1, bike2) {
		t.Error("Collision should have been detected")
	}

	bike2[0][0] = vector.Point{X: -8.5, Y: 2., Z: 0.}
	bike2[1][0] = vector.Point{X: -6.5, Y: 2., Z: 0.}
	bike2[2][0] = vector.Point{X: -8.5, Y: 0., Z: 0.}
	bike2[3][0] = vector.Point{X: -6.5, Y: 0., Z: 0.}

	bike2[0][1] = vector.Point{X: -6.5, Y: 2., Z: 2.}
	bike2[1][1] = vector.Point{X: -4.5, Y: 2., Z: 2.}
	bike2[2][1] = vector.Point{X: -6.5, Y: 0., Z: 2.}
	bike2[3][1] = vector.Point{X: -4.5, Y: 0., Z: 2.}

	if CollisionWithSideBike(bike1, bike2) {
		t.Error("Collision should note have been detected")
	}

}
