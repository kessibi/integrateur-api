package auth

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-pg/pg"
)

var secret = []byte(os.Getenv("SECRET"))

// JwtUserTimeout is the number of minutes defined when generating a user JWT
var JwtUserTimeout, _ = strconv.Atoi(os.Getenv("JWT_USER_TIMEOUT"))

// JwtServerTimeout is the number of minutes defined when generating a server JWT
var JwtServerTimeout, _ = strconv.Atoi(os.Getenv("JWT_SERVER_TIMEOUT"))

const timeoutDelay = 10

// Profile is the returned structure after register / login
type Profile struct {
	Name  string
	Token string
}

// User struct in DB
type User struct {
	ID         int     `sql:"id" json:"-"`
	UUID       string  `sql:"uuid" json:",omitempty"`
	Username   string  `sql:"login"`
	Password   string  `sql:"password" json:"-"`
	KillCount  int     `sql:"killcount"`
	DeathCount int     `sql:"deathcount"`
	KD         float32 `sql:"-"`
}

// Res is the login function return value
type Res struct {
	Login bool
}

// GameServer contains informations about a remote game server
type GameServer struct {
	Name     string `json:",omitempty"`
	IP       string `json:",omitempty"`
	Port     string `json:",omitempty"`
	Token    string `json:",omitempty"`
	Hash     string `json:",omitempty"`
	Players  int
	Users    map[string]*UserStats `json:"-"`
	LastSeen time.Time             `json:"-"`
}

// UserStats contains kill and death counters and the K/D ratio
type UserStats struct {
	Username   string
	KillCount  int
	DeathCount int
	KD         float32
}

//write "hello, world" to the given http writer
func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "hello, world")
}

//write "hello, world" to the given http writer if it succesfully registered.
func helloRegistered(w http.ResponseWriter, r *http.Request) {
	reqToken := strings.Split(r.Header.Get("Authorization"), "Bearer ")[1]
	_, err := jwt.Parse(reqToken, func(token *jwt.Token) (interface{}, error) {
		return []byte(secret), nil
	})
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintln(w, err)
	} else {
		fmt.Fprintln(w, "hello, world")
	}
}

func register(w http.ResponseWriter, r *http.Request) {
	//* decode json
	if r.Body == nil {
		http.Error(w, "", http.StatusBadRequest)
		return
	}
	var data registerData
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, "", 400)
		return
	}

	db := pg.Connect(&dbOpts)
	defer db.Close()

	// insert user

	user := &User{
		Username: data.Username,
		Password: data.Password,
	}

	err = db.Insert(user)
	if err != nil {
		log.Println("db insert: ", err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	user.Password = ""
	err = db.Select(user)
	if err != nil {
		log.Println("db select: ", err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	claims := &jwt.StandardClaims{ // create claims based on request
		Id:        user.UUID,
		IssuedAt:  time.Now().Unix(),
		ExpiresAt: time.Now().Add(time.Minute * time.Duration(JwtUserTimeout)).Unix(),
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims) // gen token
	signed, err := token.SignedString(secret)                  // sign token
	if err != nil {
		log.Println("error signing jwt", err)
	}
	js, err := json.Marshal(Profile{
		Name:  user.Username,
		Token: signed,
	})
	if err != nil {
		log.Println(err)
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

// try to log a user by finding him on the database and checking his entries
func login(w http.ResponseWriter, r *http.Request) {
	//* decode json
	if r.Body == nil {
		http.Error(w, "", http.StatusBadRequest)
		return
	}
	var data loginData
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, "", 400)
		return
	}

	db := pg.Connect(&dbOpts)
	defer db.Close()
	foo := Res{}
	_, err = db.Query(&foo, "SELECT login(?0, ?1)",
		data.Username,
		data.Password)

	if err != nil {
		log.Println("db select: ", err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	if !foo.Login {
		http.Error(w, "", http.StatusUnauthorized)
		return
	}

	claims := &jwt.StandardClaims{ // create claims based on request
		Id:        data.Username,
		IssuedAt:  time.Now().Unix(),
		ExpiresAt: time.Now().Add(time.Minute * time.Duration(JwtUserTimeout)).Unix(),
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims) // gen token
	signed, err := token.SignedString(secret)                  // sign token
	if err != nil {
		log.Println("error signing jwt", err)
	}
	js, err := json.Marshal(Profile{
		Name:  data.Username,
		Token: signed,
	})
	if err != nil {
		log.Println(err)
	}
	log.Printf("user %s logged in", data.Username)
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func logout(w http.ResponseWriter, r *http.Request) {
	// todo
}

//used to check if a server is up (just a print)
func status(w http.ResponseWriter, r *http.Request) {
	fmt.Println(r.Header.Get("X-Forwarded-For"))
	fmt.Fprintln(w, "it works !")
}

func debug(w http.ResponseWriter, r *http.Request) {
	log.Println(strings.Split(r.Header.Get("Authorization"), "Bearer "))
	fmt.Fprintln(w, "foo")
}

//add a server to usable game server and give it access tokens
func (srv *Server) serverRegister(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm() // parse request
	if err != nil {
		log.Println("parseform", err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	//* check if UDP is reachable
	buf := make([]byte, 5)

	// conn, err := net.Dial("udp", net.JoinHostPort(addr, r.Form.Get("port")))
	port, err := strconv.Atoi(r.Form.Get("port"))
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	addr := r.Header.Get("X-Forwarded-For")
	if addr == "" {
		addr, _, err = net.SplitHostPort(r.RemoteAddr) // parse remote address
		if err != nil {
			log.Println(err)
			http.Error(w, "", http.StatusInternalServerError)
			return
		}
	}

	// send echo packet to check if server is reachable
	lcl := &net.UDPAddr{IP: net.ParseIP("0.0.0.0"), Port: 35420}
	rmt := &net.UDPAddr{IP: net.ParseIP(addr), Port: port}
	conn, err := net.ListenUDP("udp", lcl)
	defer conn.Close()
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	_, err = conn.WriteToUDP([]byte("e"), rmt)
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	conn.SetReadDeadline(time.Now().Add(1 * time.Second))
	_, _, err = conn.ReadFromUDP(buf)
	if err != nil {
		log.Println("udp read timeout", err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	// generate server hash
	serverHash := base64.StdEncoding.EncodeToString([]byte(time.Now().String()))
	claims := &jwt.StandardClaims{ // create claims based on request
		Id:        serverHash,
		IssuedAt:  time.Now().Unix(),
		ExpiresAt: time.Now().Add(time.Minute * time.Duration(JwtServerTimeout)).Unix(),
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims) // gen token
	signed, err := token.SignedString(secret)                  // sign token
	if err != nil {
		log.Println("error signing jwt", err)
	}
	newServer := GameServer{
		Name:     r.Form.Get("name"),
		Token:    signed,
		IP:       addr,
		Port:     r.Form.Get("port"),
		Hash:     serverHash,
		Players:  0,
		LastSeen: time.Now(),
		Users:    make(map[string]*UserStats, 100),
	}
	js, err := json.Marshal(newServer)
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	newServer.Token = "" // token not stored
	srv.RemoteServers[serverHash] = &newServer
	log.Printf("UDP server \"%s\"(%s:%s) registered", newServer.Name,
		newServer.IP, newServer.Port)
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

// remove server from map
func (srv *Server) forgetServer(w http.ResponseWriter, r *http.Request) {
	reqToken := getToken(r)
	token, err := jwt.Parse(reqToken,
		func(token *jwt.Token) (interface{}, error) {
			return []byte(secret), nil
		})
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	claims := token.Claims.(jwt.MapClaims)
	delete(srv.RemoteServers, claims["jti"].(string))
	log.Printf("UDP server \"%s\" unregistered", claims["jti"])

	fmt.Fprintln(w, "ok")
}

// get servers list (and data is authenticated)
func (srv *Server) getServers(w http.ResponseWriter, r *http.Request) {
	//* parse jwt to check if the user is logged in
	reqToken := getToken(r)
	_, err := jwt.Parse(reqToken,
		func(token *jwt.Token) (interface{}, error) {
			return []byte(secret), nil
		})
	res := make([]GameServer, 0)
	timedOut := make([]string, 0)
	for k := range srv.RemoteServers { // check timeout
		if time.Since(srv.RemoteServers[k].LastSeen) >= (timeoutDelay * time.Second) {
			timedOut = append(timedOut, k)
			continue
		}
		if err != nil { // send only name and players count if not logged in
			res = append(res, GameServer{
				Name:    srv.RemoteServers[k].Name,
				Players: srv.RemoteServers[k].Players,
			})
		} else {
			res = append(res, *srv.RemoteServers[k])
		}
	}
	for i := 0; i < len(timedOut); i++ {
		log.Printf("server \"%s\" timed out", srv.RemoteServers[timedOut[i]].Name)
		delete(srv.RemoteServers, timedOut[i])
	}
	js, err := json.Marshal(res)
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

// update score
func (srv *Server) updateScore(w http.ResponseWriter, r *http.Request) {
	reqToken := getToken(r)
	token, err := jwt.Parse(reqToken,
		func(token *jwt.Token) (interface{}, error) {
			return []byte(secret), nil
		})
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusUnauthorized)
		return
	}
	err = r.ParseForm()
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	db := pg.Connect(&dbOpts)
	user := &User{}
	defer db.Close()
	err = db.Model(user).Column( // update global score in DB
		"id",
		"uuid",
		"killcount",
		"deathcount",
	).Where("login = ?", r.Form.Get("uuid")).Select()
	if err != nil {
		log.Println("select user error:", err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	_kill, err := strconv.Atoi(r.Form.Get("kill"))
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	_death, err := strconv.Atoi(r.Form.Get("death"))
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	user.KillCount += _kill
	user.DeathCount += _death
	_, err = db.Model(user).Column("killcount").Column("deathcount").WherePK().Update()
	if err != nil {
		log.Println("update score error:", err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	claims := token.Claims.(jwt.MapClaims)
	_id := claims["jti"].(string) // update local score
	newKill := srv.RemoteServers[_id].Users[r.Form.Get("uuid")].KillCount + _kill
	newDeath := srv.RemoteServers[_id].Users[r.Form.Get("uuid")].DeathCount + _death
	srv.RemoteServers[_id].Users[r.Form.Get("uuid")].KillCount = newKill
	srv.RemoteServers[_id].Users[r.Form.Get("uuid")].DeathCount = newDeath
	if newDeath <= 0 {
		srv.RemoteServers[_id].Users[r.Form.Get("uuid")].KD = float32(newKill)
	} else {
		srv.RemoteServers[_id].Users[r.Form.Get("uuid")].KD = float32(newKill) / float32(newDeath)
	}
}

// get global rankings
func (srv *Server) getRankings(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	db := pg.Connect(&dbOpts)
	defer db.Close()
	foo := []User{}
	_, err := db.Query(&foo, "SELECT * FROM rankings()")
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	res, err := json.Marshal(foo)
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(res)

}

// get local rankings
func (srv *Server) getServerRankings(w http.ResponseWriter, r *http.Request) {
	if r.Body == nil {
		http.Error(w, "", http.StatusBadRequest)
		return
	}
	var data rankingData
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, "", 400)
		return
	}

	_serv, ok := srv.RemoteServers[data.ID] // get server from hash
	if !ok {
		http.Error(w, "server not found", http.StatusInternalServerError)
		return
	}
	foo := make([]UserStats, 0, len(_serv.Users)) // build response
	for _, v := range _serv.Users {
		foo = append(foo, *v)
	}

	res, err := json.Marshal(foo)
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(res)
}

// keepalive handler
func (srv *Server) keepalive(w http.ResponseWriter, r *http.Request) {
	// verify token
	reqToken := getToken(r)
	err := r.ParseForm()
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	token, err := jwt.Parse(reqToken,
		func(token *jwt.Token) (interface{}, error) {
			return []byte(secret), nil
		})
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	claims := token.Claims.(jwt.MapClaims)
	_id := claims["jti"].(string)
	_, ok := srv.RemoteServers[_id]
	if !ok {
		// log.Println("server not found")
		http.Error(w, "server not found", http.StatusInternalServerError)
		return
	}
	srv.RemoteServers[_id].LastSeen = time.Now() // update timestamp
	players, err := strconv.Atoi(r.Form.Get("players"))
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	srv.RemoteServers[_id].Players = players // update players count
}

// link player to game server
func (srv *Server) linkPlayer(w http.ResponseWriter, r *http.Request) {
	reqToken := getToken(r)
	err := r.ParseForm()
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	token, err := jwt.Parse(reqToken,
		func(token *jwt.Token) (interface{}, error) {
			return []byte(secret), nil
		})
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	claims := token.Claims.(jwt.MapClaims)
	// init stats
	srv.RemoteServers[claims["jti"].(string)].Users[r.Form.Get("username")] = &UserStats{
		Username:   r.Form.Get("username"),
		KillCount:  0,
		DeathCount: 0,
		KD:         0,
	}
}

// remove player from server
func (srv *Server) unlinkPlayer(w http.ResponseWriter, r *http.Request) {
	reqToken := getToken(r)
	err := r.ParseForm()
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	token, err := jwt.Parse(reqToken,
		func(token *jwt.Token) (interface{}, error) {
			return []byte(secret), nil
		})
	if err != nil {
		log.Println(err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	claims := token.Claims.(jwt.MapClaims)
	_id := claims["jti"].(string)
	delete(srv.RemoteServers[_id].Users, r.Form.Get("username"))
}
