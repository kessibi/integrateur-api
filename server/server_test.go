package server

import (
	"log"
	"net"
	"strconv"
	"testing"

	pb "git.unistra.fr/integratron/integrateur-api/protobuf"
	"github.com/golang/protobuf/proto"
)

const UDPEndpointAddr = "127.0.0.1"
const UDPEndpointPort = 1234

//initialise udp server and start it
func init() {
	udp, err := CreateServer("udp", UDPEndpointAddr, UDPEndpointPort, "foo")
	if err != nil {
		log.Println("error starting UDP server")
		return
	}

	go func() {
		udp.Run()
	}()
}
func TestUDPServer_Running(t *testing.T) {
	server := UDPServer{
		addr: UDPEndpointAddr,
		port: UDPEndpointPort,
	}

	conn, err := net.Dial("udp", server.addr+":"+strconv.Itoa(server.port))
	if err != nil {
		t.Error("dial error:", err)
	}
	conn.Close()
}

func TestUDPServer_SendMessage(t *testing.T) {
	server := UDPServer{
		addr: UDPEndpointAddr,
		port: UDPEndpointPort,
	}
	game := &pb.ClientToServ{}

	data, err := proto.Marshal(game)
	if err != nil {
		t.Error("Marshal error:", err)
	}

	conn, err := net.Dial("udp", server.addr+":"+strconv.Itoa(server.port))
	if err != nil {
		t.Error("dial error:", err)
	}
	defer conn.Close()

	_, err = conn.Write(data)
	if err != nil {
		t.Error("write:", err)
	}
}
