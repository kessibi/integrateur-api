package server

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"git.unistra.fr/integratron/integrateur-api/collision"

	"git.unistra.fr/integratron/integrateur-api/vector"

	"git.unistra.fr/integratron/integrateur-api/auth"
	"git.unistra.fr/integratron/integrateur-api/barrier"

	pb "git.unistra.fr/integratron/integrateur-api/protobuf"
	"github.com/golang/protobuf/proto"
)

// UserData contains all datas representing an user
type UserData struct {
	Remote   *net.UDPAddr
	SndSeq   int32
	RcvSeq   int32
	Pos      pb.ServToClientEnemy
	Bar      barrier.Barrier
	FP       collision.FrontPoint
	LastSeen time.Time
	Killer   string
	UUID     string
}

// UserIndex maps an UUID with the corresponding index of the UserData tab
type UserIndex struct {
	Map    sync.Map
	Clocks sync.Map
	Next   int
	Last   int
	Mutex  sync.Mutex
	Count  int
}

// MaxSeq is the max sequence number
const MaxSeq = 4096

// TickRate is the delay of a server tick in milliseconds
var TickRate = 35

const deadTimeout = 2
const disconnectTimeout = 5

var authURL = os.Getenv("AUTH_ENDPOINT")
var maxBarrierLength = 200

var idxLock [100]sync.RWMutex

// Content contains information about itself(server)
var httpClient http.Client
var contentServ auth.GameServer
var contentMutex sync.RWMutex

/* Main Handlers */

// HandleConnections is the main loop of the UDP Server,
// it parses received Protobuf messages
func (u *UDPServer) HandleConnections() {
	_tick, err := strconv.Atoi(os.Getenv("TICK_RATE"))
	if err == nil {
		TickRate = _tick
	}

	_nHandlers, err := strconv.Atoi(os.Getenv("HANDLERS"))
	if err != nil {
		_nHandlers = runtime.NumCPU()
	}

	_bLen, err := strconv.Atoi(os.Getenv("BARRIER_LENGTH"))
	if err == nil {
		maxBarrierLength = _bLen
	}

	var data [100]UserData

	userIndex := UserIndex{
		Map:    sync.Map{},
		Clocks: sync.Map{},
		Next:   0,
	}

	for i := 0; i < _nHandlers; i++ {
		go u.clientHandler(&data, &userIndex) //

	}
	go u.ticker(&data, &userIndex) //

	httpClient := &http.Client{
		Timeout: time.Second * 1,
	}
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	if authURL == "" {
		authURL = "https://integratron.paul-heng.fr"
	}
	log.Println("connecting to authentication server at " + authURL)
	resp, err := httpClient.PostForm(authURL+"/regserver",
		url.Values{"name": {u.name}, "port": {strconv.Itoa(u.port)}})

	if err != nil {
		log.Println(err)
	} else {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println("ioutils:", err)
		}
		err = json.Unmarshal(body, &contentServ)
		if err != nil {
			log.Println("json", err)
		}
	}
	defer func(token string) {
		req, _ := http.NewRequest("GET", authURL+"/forget", nil)
		req.Header.Set("Authorization", strings.Join([]string{
			"Bearer ",
			token,
		}, ""))
		httpClient.Do(req)

	}(contentServ.Token)
	go u.keepalive()
	<-signals

	return
}

/* Test Functions */

// sendEcho sends back the received message
func (u *UDPServer) sendEcho(addr *net.UDPAddr, content []byte) {
	_, err := u.server.WriteToUDP(content, addr)
	if err != nil {
		log.Printf("could not send echo response: %v", err)
	}
}

/* Server Functions */

/*
 * routine managing users data
 * parses packets from clients to get their barrier and position
 *
 */
func (u *UDPServer) clientHandler(data *[100]UserData, hosts *UserIndex) {
	buf := make([]byte, BUFSIZE)
	msg := &pb.ClientToServ{}
	bar := &pb.Barrier{}
	log.Println("handler started")

	for {

		// read from UDP if packets are available
		n, remoteaddr, err := u.server.ReadFromUDP(buf)
		if err != nil {
			log.Println(err)
			continue
		}
		if remoteaddr == nil {
			continue
		}

		// determine which kind of message based on the prefix then unmarshal
		if buf[0] == 'e' { //* echo for udp server registration
			u.server.WriteToUDP([]byte("e"), remoteaddr)
			continue
		} else if buf[0] == 'b' { // barrier
			err = proto.Unmarshal(buf[1:n], bar)
			if err != nil {
				log.Println(err)
				continue
			}

			t, ok := hosts.Clocks.Load(bar.UUID)
			if ok {
				if time.Since(t.(time.Time)) >= (deadTimeout * time.Second) {
					hosts.Clocks.Delete(bar.UUID)
				} else {
					continue
				}
			}

			_id, ok := hosts.Map.Load(bar.UUID)
			if !ok {
				continue
			}
			idxLock[_id.(int)].Lock()
			if data[_id.(int)].Bar.CurrLen == 0 && bar.SequenceNumber == 0 {
				idxLock[_id.(int)].Unlock()
				continue
			}
			barrier.AddPointToBarrier(
				&data[_id.(int)].Bar,
				vector.Point(vector.FromProto(bar.LowerPoint)),
				vector.Point(vector.FromProto(bar.UpperPoint)),
				int(bar.SequenceNumber),
			)
			idxLock[_id.(int)].Unlock()
			continue
		} else if buf[0] == 'm' { // message
			err = proto.Unmarshal(buf[1:n], msg)
			if err != nil {
				log.Println(err)
				continue
			}
		} else { // unknown => discard
			log.Println("unknown message type received: prefix", buf[0])
			continue
		}
		// if an ID/UUID is not in the hosts, adds it
		initState := pb.ServToClient_DEAD
		_idx, loaded := hosts.Map.LoadOrStore(msg.UUID, hosts.Next)
		idx := _idx.(int)
		if !loaded {
			hosts.Mutex.Lock()
			if hosts.Next >= hosts.Last && hosts.Last < 99 {
				hosts.Last++
			}
			for i := 0; i <= hosts.Last; i++ {
				idxLock[i].RLock()
				if i != idx && data[i].Remote == nil {
					hosts.Next = i
					idxLock[i].RUnlock()
					break
				}
				idxLock[i].RUnlock()
			}
			hosts.Clocks.Store(msg.UUID, time.Now())
			hosts.Mutex.Unlock()
			idxLock[idx].Lock()
			data[idx].Remote = remoteaddr
			data[idx].RcvSeq = 0
			data[idx].SndSeq = 0
			data[idx].LastSeen = time.Now()
			data[idx].Killer = ""
			data[idx].UUID = msg.UUID
			barrier.InitBarrier(&data[idx].Bar, maxBarrierLength, 0, idx)
			idxLock[idx].Unlock()
			go addPlayerToServer(msg.UUID)
			go log.Println("New host joined:", remoteaddr)
		}

		_, ok := hosts.Clocks.Load(msg.UUID)
		if !ok {
			initState = pb.ServToClient_ALIVE
		}

		//if data[idx].Remote == remoteaddr {
		idxLock[idx].RLock()
		lastSeq := data[idx].RcvSeq
		idxLock[idx].RUnlock()

		if (msg.SequenceNumber > lastSeq) ||
			(msg.SequenceNumber < lastSeq%MaxSeq/2) {
			_new := pb.ServToClientEnemy{
				State: initState,
				Mat:   msg.Mp,
			}

			if !ok && checkCollisions(data, hosts, *msg.Mp, idx, msg.UUID) {
				go addDeath(msg.UUID)
				_new.State = pb.ServToClient_DEAD
				idxLock[idx].Lock()
				_new.Killer = data[idx].Killer
				barrier.FreeBarrier(&data[idx].Bar)
				hosts.Mutex.Lock()
				hosts.Clocks.Store(msg.UUID, time.Now())
				hosts.Mutex.Unlock()
			} else {
				idxLock[idx].Lock()
			}

			data[idx].Pos = _new
			data[idx].LastSeen = time.Now()
			data[idx].RcvSeq = msg.SequenceNumber
			idxLock[idx].Unlock()
		}
	}
}

// send all players data to all clients
func (u *UDPServer) ticker(data *[100]UserData, hosts *UserIndex) {
	for {
		res := &pb.ServToClient{
			EnemyState: buildMap(data, hosts),
		}
		hosts.Mutex.Lock()
		_last := hosts.Last
		hosts.Mutex.Unlock()
		for i := 0; i < _last; i++ { // send response to all hosts
			idxLock[i].RLock()
			if data[i].Remote == nil {
				idxLock[i].RUnlock()
				continue
			}
			addr := data[i].Remote
			res.SequenceNumber = (data[i].SndSeq + 1) % MaxSeq
			idxLock[i].RUnlock()
			idxLock[i].Lock()
			data[i].SndSeq = res.SequenceNumber
			idxLock[i].Unlock()
			content, err := proto.Marshal(res)
			if err != nil {
				log.Println("error during marshal:", err)
			}

			_, err = u.server.WriteToUDP(content, addr)
			if err != nil {
				log.Println("could not send response: ", err)
			}
		}
		// log.Println("sent")

		time.Sleep(time.Duration(TickRate) * time.Millisecond)
	}
}

// prepare map for message transfer according to protobuf definition
func buildMap(data *[100]UserData, hosts *UserIndex) map[string]*pb.ServToClientEnemy {
	res := make(map[string]*pb.ServToClientEnemy)
	var toDelete string
	hosts.Map.Range(func(key, val interface{}) bool {
		idxLock[val.(int)].Lock()
		if time.Since(data[val.(int)].LastSeen) >= (disconnectTimeout * time.Second) {
			barrier.FreeBarrier(&data[val.(int)].Bar)
			data[val.(int)] = UserData{}
			idxLock[val.(int)].Unlock()
			toDelete = key.(string)
			return true
		}
		pos := data[val.(int)].Pos
		idxLock[val.(int)].Unlock()
		res[key.(string)] = &pos
		return true
	})
	if len(toDelete) != 0 {
		hosts.Map.Delete(toDelete)
		go removePlayerFromServer(toDelete)
		log.Println(toDelete, "timeout")
	}
	return res
}

//test collisions for a given player against every barrier
func checkCollisions(data *[100]UserData, hosts *UserIndex, pos pb.MotoPosition, id int, uuid string) bool {
	// initialize parameters
	mat := [4]vector.Vector{
		vector.FromProto(pos.X),
		vector.FromProto(pos.Y),
		vector.FromProto(pos.Z),
		vector.FromProto(pos.P),
	}
	hosts.Mutex.Lock()
	_last := hosts.Last
	hosts.Mutex.Unlock()
	idxLock[id].Lock()
	collision.NewFrontPoint(&data[id].FP, mat)
	if !collision.IsFastEnough(vector.FromProto(pos.Speed)) && pos.P.J < 200 {
		idxLock[id].Unlock()
		fmt.Println("himself", id)
		return true
	}
	idxLock[id].Unlock()
	for i := 0; i < _last; i++ {
		idxLock[i].RLock()
		if data[i].Remote == nil {
			idxLock[i].RUnlock()
			continue
		}
		copy := data[i].Bar
		remoteUUID := data[i].UUID
		idxLock[i].RUnlock()
		idxLock[id].Lock()
		if collision.BikeHasACollision(copy, &data[id].FP) && pos.P.J < 200 {
			fmt.Println("id =", id, "i =", i)
			hosts.Mutex.Lock()
			data[id].Killer = ""
			if id != i {
				data[id].Killer = remoteUUID
				go addKill(i, &hosts.Map)
			}
			hosts.Mutex.Unlock()
			idxLock[id].Unlock()
			return true
		}
		idxLock[id].Unlock()
	}
	return false
}

func addKill(id int, m *sync.Map) {
	uuid, ok := mapkey(m, id)
	if ok {
		sendUpdateScore(uuid, 1, 0)
	}
}
func addDeath(uuid string) {
	sendUpdateScore(uuid, 0, 1)
}

func sendUpdateScore(uuid string, kill, death int) {
	contentMutex.Lock()
	form := url.Values{}
	form.Set("uuid", uuid)
	form.Set("kill", strconv.Itoa(kill))
	form.Set("death", strconv.Itoa(death))
	req, err := http.NewRequest("POST", authURL+"/upscore", strings.NewReader(form.Encode()))
	req.Header.Set("Authorization", strings.Join([]string{
		"Bearer ",
		contentServ.Token,
	}, ""))
	if err != nil {
		log.Println(err)
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	httpClient.Do(req)
	contentMutex.Unlock()
}

func (u *UDPServer) keepalive() {
	log.Println("keepalive sender started")
	for {
		contentMutex.RLock()
		form := url.Values{}
		form.Set("players", strconv.Itoa(contentServ.Players))
		req, _ := http.NewRequest("POST", authURL+"/keepalive", strings.NewReader(form.Encode()))
		req.Header.Set("Authorization", strings.Join([]string{
			"Bearer ",
			contentServ.Token,
		}, ""))
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

		httpClient.Do(req)
		contentMutex.RUnlock()
		time.Sleep(5 * time.Second)
	}

}

func addPlayerToServer(username string) {
	contentMutex.Lock()
	form := url.Values{}
	form.Set("username", username)
	req, _ := http.NewRequest("POST", authURL+"/linkplayer", strings.NewReader(form.Encode()))
	req.Header.Set("Authorization", strings.Join([]string{
		"Bearer ",
		contentServ.Token,
	}, ""))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	httpClient.Do(req)
	contentServ.Players++
	contentMutex.Unlock()
}

func removePlayerFromServer(username string) {
	contentMutex.Lock()
	form := url.Values{}
	form.Set("username", username)
	req, _ := http.NewRequest("POST", authURL+"/unlinkplayer", strings.NewReader(form.Encode()))
	req.Header.Set("Authorization", strings.Join([]string{
		"Bearer ",
		contentServ.Token,
	}, ""))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	httpClient.Do(req)
	contentServ.Players--
	contentMutex.Unlock()
}

func mapkey(m *sync.Map, value int) (key string, ok bool) {
	(*m).Range(func(k, v interface{}) bool {
		if v == value {
			key = k.(string)
			ok = true
			return false
		}
		return true
	})
	return
}
