package auth

type loginData struct {
	Username string
	Password string
}
type registerData struct {
	Username string
	Password string
}

type rankingData struct {
	ID string
}

//init all http request available
func (s *Server) routes() {
	s.Router.HandleFunc("/login", login).Methods("POST")
	s.Router.HandleFunc("/register", register).Methods("POST")
	s.Router.HandleFunc("/welcome", helloRegistered).Methods("GET")
	s.Router.HandleFunc("/status", status).Methods("GET")
	s.Router.HandleFunc("/debug", debug).Methods("GET")
	s.Router.HandleFunc("/regserver", s.serverRegister).Methods("POST")
	s.Router.HandleFunc("/forget", s.forgetServer).Methods("GET")
	s.Router.HandleFunc("/servers", s.getServers).Methods("GET")
	s.Router.HandleFunc("/upscore", s.updateScore).Methods("POST")
	s.Router.HandleFunc("/rankings", s.getRankings).Methods("GET")
	s.Router.HandleFunc("/servrankings", s.getServerRankings).Methods("POST")
	s.Router.HandleFunc("/keepalive", s.keepalive).Methods("POST")
	s.Router.HandleFunc("/linkplayer", s.linkPlayer).Methods("POST")
	s.Router.HandleFunc("/unlinkplayer", s.unlinkPlayer).Methods("POST")
}

// InitAuthRoutes initializes the authentication routes
func (s *Server) InitAuthRoutes() {
	s.routes()
}
