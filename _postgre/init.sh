#!/bin/bash
set -e

psql -U postgres -f CREATE_TABLE.sql &&\
psql -U postgres -f CREATE_FUNCTIONS.sql &&\
psql -U postgres -f CREATE_TRIGGERS.sql