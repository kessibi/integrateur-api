FROM rahveiz/golang-mod:1.12-alpine

RUN addgroup -S integratron && adduser -S integratron -G integratron

LABEL "maintainer"="Paul HENG <paul.heng@etu.unistra.fr>"

WORKDIR /go/src/git.unistra.fr/integratron/integrateur-api/

COPY ./server /go/src/git.unistra.fr/integratron/integrateur-api/server
COPY ./main /go/src/git.unistra.fr/integratron/integrateur-api/main
COPY ./protobuf /go/src/git.unistra.fr/integratron/integrateur-api/protobuf
COPY ./vector /go/src/git.unistra.fr/integratron/integrateur-api/vector
COPY ./collision /go/src/git.unistra.fr/integratron/integrateur-api/collision
COPY ./barrier /go/src/git.unistra.fr/integratron/integrateur-api/barrier
COPY ./auth /go/src/git.unistra.fr/integratron/integrateur-api/auth
COPY ./install.sh ./install.sh

RUN chmod +x ./install.sh
ENV AUTH_ENDPOINT https://example.com

RUN ./install.sh

USER integratron

CMD ["main"]
