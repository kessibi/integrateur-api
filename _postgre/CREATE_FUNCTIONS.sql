
-- =============================================
-- Author:      Biscay Robin
-- Description: hash password at insert or update time
-- Parameters: none -> used by trigger
-- =============================================
CREATE OR REPLACE FUNCTION hash_pass() RETURNS TRIGGER AS
$$
DECLARE
    a users%ROWTYPE;
BEGIN
    SELECT * from users INTO a  where id = NEW.id;
    if a.id = NEW.id then
        UPDATE users 
        SET password = crypt(NEW.password, gen_salt('md5')) 
        WHERE id = a.id;
        RETURN NULL;
    else
        NEW.password = crypt(NEW.password, gen_salt('md5'));
        RETURN NEW;
    end if;
END;
$$
language plpgsql;

-- =============================================
-- Author:      Biscay Robin
-- Description: verify that users informations are correct
-- Parameters: 
--      @usrname - login of the person trying to connect
--      @pswd - password of the person trying to connect
-- Returns: boolean true if both information are correct
-- =============================================
CREATE OR REPLACE FUNCTION login(usrname VARCHAR(255), pswd VARCHAR(255)) RETURNS boolean AS
$$
BEGIN
    IF EXISTS(
        SELECT * 
        FROM users 
        WHERE login = usrname AND password = crypt(pswd, password)
    ) THEN
        RETURN TRUE;
    END IF;
        RETURN FALSE;
END;
$$
language plpgsql;

-- =============================================
-- Author:      Heng Paul
-- Description: returns users ordered by K/D ratio (computed)
-- Returns: table
-- =============================================
CREATE OR REPLACE FUNCTION rankings() RETURNS TABLE (
	login users.login%TYPE,
	killcount users.killcount%TYPE,
	deathcount users.deathcount%TYPE,
	kd REAL
) AS $$
BEGIN
	RETURN QUERY
	SELECT
		u.login,
		u.killcount,
		u.deathcount,
		(u.killcount::REAL / greatest(u.deathcount::REAL, 1)) AS kd
	FROM users u
	ORDER BY kd DESC;
END
$$ LANGUAGE plpgsql;